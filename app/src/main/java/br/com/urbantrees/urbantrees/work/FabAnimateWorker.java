package br.com.urbantrees.urbantrees.work;


import android.app.Activity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import br.com.urbantrees.urbantrees.R;

public class FabAnimateWorker {

    private Activity activity;
    private Animation alphaOpen, alphaClose, fabOpen, fabClose, rotateForward, rotateBackward;

    private FabRadiusClickListener fabRadiusClickListener;

    public FabAnimateWorker(Activity activity) {
        this.activity = activity;
        loadAnimations();
    }

    private void loadAnimations() {
        alphaOpen = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.alpha_open);
        alphaClose = AnimationUtils.loadAnimation(activity.getApplicationContext(),R.anim.alpha_close);
        fabOpen = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.fab_open);
        fabClose = AnimationUtils.loadAnimation(activity.getApplicationContext(),R.anim.fab_close);
        rotateForward = AnimationUtils.loadAnimation(activity.getApplicationContext(),R.anim.rotate_forward);
        rotateBackward = AnimationUtils.loadAnimation(activity.getApplicationContext(),R.anim.rotate_backward);
    }

    public void prepare() {
        prepareFabMain();
        prepareFabRadius();
    }

    private void prepareFabMain() {
        activity.findViewById(R.id.fabMain).setOnClickListener(new FabMainClickListener());
    }

    private void prepareFabRadius() {
        this.fabRadiusClickListener =  new FabRadiusClickListener();
        activity.findViewById(R.id.fabRadius).setOnClickListener(fabRadiusClickListener);
    }

    private class FabMainClickListener implements View.OnClickListener {

        final int[] ids = {R.id.fabFilter, R.id.fabRadius, R.id.fabRefresh, R.id.fabNewTreeEntry};
        private boolean open;

        @Override
        public void onClick(View view) {
            if(open){
                view.startAnimation(rotateBackward);
                for (int id : ids)
                    changeStatus(id, fabClose, false);
                fabRadiusClickListener.close();
            } else {
                view.startAnimation(rotateForward);
                for (int id : ids)
                    changeStatus(id, fabOpen, true);
            }

            open = !open;
        }
    }

    private class FabRadiusClickListener implements View.OnClickListener {

        final int[] ids = {R.id.seekRadius, R.id.textKm};

        private boolean open;

        @Override
        public void onClick(View view) {
            if (open)
                close();
            else
                open();
        }

        private void close() {
            if (!open)
                return;
            for (int id : ids)
                changeStatus(id, alphaClose, false);
            open = false;
        }

        private void open() {
            if (open)
                return;
            for (int id : ids)
                changeStatus(id, alphaOpen, true);
            open = true;
        }
    }

    private void changeStatus(int fabId, Animation anim, boolean clickable) {
        View view = activity.findViewById(fabId);
        view.startAnimation(anim);
        view.setClickable(clickable);
    }

}
