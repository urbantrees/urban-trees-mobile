package br.com.urbantrees.urbantrees.work;

import android.app.Activity;
import android.location.Location;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.drive.query.Filter;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.alfa.AlfaInterceptor;
import br.com.urbantrees.urbantrees.bean.OwnerType;
import br.com.urbantrees.urbantrees.bean.Tree;
import br.com.urbantrees.urbantrees.bean.TreeEntry;
import br.com.urbantrees.urbantrees.bean.parcel.FilterData;
import br.com.urbantrees.urbantrees.bean.parcel.NewTreeEntryData;
import br.com.urbantrees.urbantrees.json.JsonPostParser;
import br.com.urbantrees.urbantrees.json.JsonRequestParser;

public class TreeEntryWorker {

    private Activity activity;

    public TreeEntryWorker(Activity activity) {
        this.activity = activity;
    }

    public void requestTreeEntries(Location location, FilterData filterData, final RequestTreeEntryCallback callback) {
        String url = queryBuilder(location, filterData);
        final RequestQueue queue = Volley.newRequestQueue(activity);

        queue.add(
                new JsonRequestParser<TreeEntry[]>(Request.Method.GET, url, new Response.Listener<TreeEntry[]>() {
                    @Override
                    public void onResponse(TreeEntry[] response) {
                        callback.onCallback(Arrays.asList(response));
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(activity, R.string.try_again, Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    protected Class<TreeEntry[]> getParseClass() {
                        return TreeEntry[].class;
                    }
                });

        queue.start();
    }

    private String queryBuilder(Location location, FilterData filterData) {
        int radius = AlfaInterceptor.getRadius(activity, filterData.getRadius());
        String url = AlfaInterceptor.getTreeEntriesUrl(activity, String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()), radius);

        StringBuilder builder = new StringBuilder(url);
        if (isValid(filterData.getName()))
            builder.append("&").append("name=").append(filterData.getName());
        if (filterData.getCategory() != null)
            builder.append("&").append("categories=").append(filterData.getCategory().getDb());
        if (filterData.getOwnerType() != null)
            builder.append("&").append("ownerType=").append(filterData.getOwnerType().getDb());

        return builder.toString();
    }

    private boolean isValid(String value) {
        return value != null && !value.isEmpty();
    }

    public void createNewTreeEntry(NewTreeEntryData data, TreeWorker treeWorker) {
        TreeEntry entry = new TreeEntry();
        entry.setCreationDate(new Date());

        Tree tree = data.isiDunnoTheTree() ? treeWorker.getNormalTree() : treeWorker.getTree(data.getName());
        if (tree == null)
            throw new RuntimeException(activity.getString(R.string.invalid_tree));
        entry.setTree(tree);

        entry.setSender(new AccountWorker(activity).getGoogleAccount());
        entry.setOwnerType(OwnerType.valueOf(activity, data.getOwnerType()));
        entry.setLat(data.getLatLng().latitude);
        entry.setLng(data.getLatLng().longitude);

        sendTreeEntry(entry);
    }

    private void sendTreeEntry(TreeEntry entry) {
        Toast.makeText(activity, R.string.sending_tree_entry, Toast.LENGTH_SHORT).show();

        //activity.getResources().getString(R.string.put_tree_entry_url);
        String url = AlfaInterceptor.postTreeEntryUrl(activity);
        final RequestQueue queue = Volley.newRequestQueue(activity);

        queue.add(new JsonPostParser(activity, url, entry, R.string.tree_entry_was_created));
        queue.start();
    }

    public interface RequestTreeEntryCallback {
        void onCallback(List<TreeEntry> entries);
    }

}
