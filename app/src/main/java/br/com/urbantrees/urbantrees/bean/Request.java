package br.com.urbantrees.urbantrees.bean;

public class Request {

    private String sender;
    private String message;
    private TreeEntry entry;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TreeEntry getEntry() {
        return entry;
    }

    public void setEntry(TreeEntry entry) {
        this.entry = entry;
    }
}
