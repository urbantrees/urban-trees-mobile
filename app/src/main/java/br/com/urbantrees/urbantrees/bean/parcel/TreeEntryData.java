package br.com.urbantrees.urbantrees.bean.parcel;

import android.os.Parcel;
import android.os.Parcelable;

public class TreeEntryData implements Parcelable {

    private String name;
    private String scientific;
    private String creation;
    private String nicks;
    private String categories;
    private String ownerType;
    private String lat;
    private String lng;

    public TreeEntryData() {}

    protected TreeEntryData(Parcel in) {
        name = in.readString();
        scientific = in.readString();
        creation = in.readString();
        nicks = in.readString();
        categories = in.readString();
        ownerType = in.readString();
        lat = in.readString();
        lng = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(scientific);
        dest.writeString(creation);
        dest.writeString(nicks);
        dest.writeString(categories);
        dest.writeString(ownerType);
        dest.writeString(lat);
        dest.writeString(lng);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TreeEntryData> CREATOR = new Creator<TreeEntryData>() {
        @Override
        public TreeEntryData createFromParcel(Parcel in) {
            return new TreeEntryData(in);
        }

        @Override
        public TreeEntryData[] newArray(int size) {
            return new TreeEntryData[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScientific() {
        return scientific;
    }

    public void setScientific(String scientific) {
        this.scientific = scientific;
    }

    public String getCreation() {
        return creation;
    }

    public void setCreation(String creation) {
        this.creation = creation;
    }

    public String getNicks() {
        return nicks;
    }

    public void setNicks(String nicks) {
        this.nicks = nicks;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

}
