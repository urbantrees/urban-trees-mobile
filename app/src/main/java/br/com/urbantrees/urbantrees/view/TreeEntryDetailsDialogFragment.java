package br.com.urbantrees.urbantrees.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.bean.Category;
import br.com.urbantrees.urbantrees.bean.TreeEntry;
import br.com.urbantrees.urbantrees.bean.parcel.ReportEntryData;
import br.com.urbantrees.urbantrees.bean.parcel.TreeEntryData;
import br.com.urbantrees.urbantrees.work.AccountWorker;

public class TreeEntryDetailsDialogFragment extends DialogFragment {

    public static final String TREE_ENTRY_DIALOG = "treeEntryDialog";
    private static final String KEY_DATA = "treeEntryData";

    private View dialogView;
    private TreeEntryData data;

    public static TreeEntryDetailsDialogFragment newInstance(Context context, TreeEntry entry) {
        TreeEntryDetailsDialogFragment fragment = new TreeEntryDetailsDialogFragment();
        TreeEntryData data = createData(context, entry);
        Bundle args = new Bundle();
        args.putParcelable(KEY_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    private static TreeEntryData createData(Context context, TreeEntry entry) {
        TreeEntryData data = new TreeEntryData();
        data.setName(entry.getTree().getName());
        data.setScientific(entry.getTree().getScientificName());
        data.setCreation(new SimpleDateFormat("dd/MM/yy HH:mm").format(entry.getCreationDate()));
        data.setNicks(normalize(entry.getTree().getNicks()));
        data.setCategories(normalize(Category.getNames(context, entry.getTree().getCategories())));
        data.setOwnerType(entry.getOwnerType() == null ? "" : entry.getOwnerType().stringResource(context));
        data.setLat(String.valueOf(entry.getLat()));
        data.setLng(String.valueOf(entry.getLng()));
        return data;
    }

    private static String normalize(List list) {
        return list.toString().replace("[", "").replace("]", "");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_DATA, this.data);
        super.onSaveInstanceState(outState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        loadSavedInstanceState(savedInstanceState);
        dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_tree_entry_details, null);
        setViews();
       return createDialog();
    }

    private void loadSavedInstanceState(Bundle savedInstanceState) {
        Bundle state = savedInstanceState == null ? getArguments() : savedInstanceState;
        this.data = state.getParcelable(KEY_DATA);
    }

    private void setViews() {
        setTextViews();
        setBtnReportEntry();
    }

    private void setTextViews() {
        ((TextView) dialogView.findViewById(R.id.textScientific)).setText(data.getScientific());
        ((TextView) dialogView.findViewById(R.id.textCreation)).setText(data.getCreation());

        ((TextView) dialogView.findViewById(R.id.textNicks)).setText(data.getNicks());
        ((TextView) dialogView.findViewById(R.id.textCategories)).setText(data.getCategories());
        ((TextView) dialogView.findViewById(R.id.textOwnerType)).setText(data.getOwnerType());

        ((TextView) dialogView.findViewById(R.id.textLatitude)).setText(data.getLat());
        ((TextView) dialogView.findViewById(R.id.textLongitude)).setText(data.getLng());
    }

    private void setBtnReportEntry() {
        Button btnReportEntry = (Button) dialogView.findViewById(R.id.btnReportEntry);
        btnReportEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!new AccountWorker(getActivity()).validateGoogleAccount(dialogView))
                    return;

                ReportEntryData reportData = new ReportEntryData();
                reportData.setTreeName(data.getName());
                reportData.setLat(data.getLat());
                reportData.setLng(data.getLng());

                ReportTreeEntryRequestDialogFragment dialog = ReportTreeEntryRequestDialogFragment.newInstance(reportData);
                dialog.show(getActivity().getSupportFragmentManager(), ReportTreeEntryRequestDialogFragment.REQUEST_REPORT_ENTRY_DIALOG);
            }
        });
    }

    private AlertDialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(data.getName());
        builder.setView(dialogView);
        builder.setPositiveButton(R.string.ok, null);

        return builder.create();
    }

}
