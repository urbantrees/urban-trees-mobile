package br.com.urbantrees.urbantrees.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.bean.OwnerType;
import br.com.urbantrees.urbantrees.bean.Tree;
import br.com.urbantrees.urbantrees.bean.TreeIcon;
import br.com.urbantrees.urbantrees.bean.parcel.NewTreeEntryData;

public class NewTreeEntryDialogFragment extends DialogFragment implements OnMapReadyCallback {

    public static final String NEW_TREE_ENTRY_DIALOG = "newTreeEntryDialog";

    private static final String KEY_DATA = "newEntryData";

    private View dialogView;
    private NewTreeEntryData data;
    private GoogleMap mMap;

    public static NewTreeEntryDialogFragment newInstance(NewTreeEntryData data) {
        NewTreeEntryDialogFragment fragment = new NewTreeEntryDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_DATA, this.data);
        super.onSaveInstanceState(outState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        loadSavedInstanceState(savedInstanceState);
        dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_tree_entry_new, null);

        setFields();

        return createDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        destroyMap();
    }

    private void loadSavedInstanceState(Bundle savedInstanceState) {
        Bundle state = savedInstanceState == null ? getArguments() : savedInstanceState;
        this.data = state.getParcelable(KEY_DATA);
    }

    private void setFields() {
        setBtnRequestTree();
        setTextTreeNames();
        setChkNormalTreeOnClickListener();
        setSpinnerOwnerType();
        configureMap();
    }

    private void setBtnRequestTree() {
        Button textTreeNames = (Button) dialogView.findViewById(R.id.btnRequestTree);
        textTreeNames.setOnClickListener(new RequestNewTreeListener());
    }

    private void setTextTreeNames() {
        AutoCompleteTextView textTreeNames = (AutoCompleteTextView) dialogView.findViewById(R.id.autoCompleteTreeNames);
        ArrayAdapter<Tree> treesAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, data.getMatchNames());
        textTreeNames.setAdapter(treesAdapter);

        if (data.getName() != null)
            textTreeNames.setText(data.getName());
    }

    private void setChkNormalTreeOnClickListener() {
        final CheckBox chkNormalTree = (CheckBox) dialogView.findViewById(R.id.chkIDunnoTree);
        chkNormalTree.setChecked(data.isiDunnoTheTree());

        chkNormalTree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshTextTreeNamesEnable();
            }
        });

        refreshTextTreeNamesEnable();
    }

    private void refreshTextTreeNamesEnable() {
        AutoCompleteTextView textTreeNames = (AutoCompleteTextView) dialogView.findViewById(R.id.autoCompleteTreeNames);
        final CheckBox chkNormalTree = (CheckBox) dialogView.findViewById(R.id.chkIDunnoTree);
        textTreeNames.setEnabled(!chkNormalTree.isChecked());
    }

    private void setSpinnerOwnerType() {
        Spinner spinnerOwnerType = (Spinner) dialogView.findViewById(R.id.spinnerOwnerType);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, OwnerType.getNames(getContext()));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerOwnerType.setAdapter(adapter);

        if (data.getOwnerType() != null)
            spinnerOwnerType.setSelection(adapter.getPosition(data.getOwnerType()));
    }

    private void configureMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.newTreeMap);
        mapFragment.getMapAsync(this);
    }

    private void destroyMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.newTreeMap);
        if (mapFragment != null)
            getFragmentManager().beginTransaction().remove(mapFragment).commit();
    }

    @Override
    public void onMapReady(GoogleMap mMap) {
        this.mMap = mMap;
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.map_style));
        mMap.setBuildingsEnabled(false);
        mMap.setTrafficEnabled(false);

        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            mMap.setMyLocationEnabled(true);

        UiSettings ui = mMap.getUiSettings();
        ui.setMyLocationButtonEnabled(false);
        ui.setCompassEnabled(false);
        ui.setScrollGesturesEnabled(false);
        ui.setZoomGesturesEnabled(false);
        ui.setRotateGesturesEnabled(false);

        mMap.setOnMarkerDragListener(new MarkerDragListener());

        mMap.addMarker(new MarkerOptions().position(data.getLatLng()).draggable(true).icon(TreeIcon.GREEN.getBitmapDescriptor()));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(data.getLatLng(), 17));
    }

    private AlertDialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialogView);
        builder.setTitle(R.string.add_new_tree_in_map);
        builder.setPositiveButton(R.string.create, new OkListener());
        builder.setNegativeButton(R.string.cancel, null);
        return builder.create();
    }

    private class MarkerDragListener implements GoogleMap.OnMarkerDragListener {
        @Override
        public void onMarkerDragStart(Marker m) {}

        @Override
        public void onMarkerDragEnd(Marker m) {
            data.setLatLng(m.getPosition());
            mMap.animateCamera(CameraUpdateFactory.newLatLng(m.getPosition()));
        }

        @Override
        public void onMarkerDrag(Marker arg0) {}
    }

    private class OkListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            updateData();
            ((DismissCallback) getActivity()).onDismiss(data);
        }

        private void updateData() {
            CheckBox chkNormalTree = (CheckBox) dialogView.findViewById(R.id.chkIDunnoTree);
            data.setiDunnoTheTree(chkNormalTree.isChecked());
            AutoCompleteTextView textTreeNames = (AutoCompleteTextView) dialogView.findViewById(R.id.autoCompleteTreeNames);
            data.setName(textTreeNames.getText().toString());
            Spinner spinnerOwnerType = (Spinner) dialogView.findViewById(R.id.spinnerOwnerType);
            data.setOwnerType((String) spinnerOwnerType.getSelectedItem());
        }
    }

    private class RequestNewTreeListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            NewTreeRequestDialogFragment dialog = new NewTreeRequestDialogFragment();
            dialog.show(getActivity().getSupportFragmentManager(), NewTreeRequestDialogFragment.REQUEST_TREE_DIALOG);
        }
    }

    public interface DismissCallback {
        void onDismiss (NewTreeEntryData data);
    }
}
