package br.com.urbantrees.urbantrees.bean.parcel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import br.com.urbantrees.urbantrees.bean.Category;
import br.com.urbantrees.urbantrees.bean.OwnerType;

public class FilterData implements Parcelable {

    private List<String> matchNames;
    private String name;
    private Category category;
    private OwnerType ownerType;
    private int radius;

    public FilterData(){}

    protected FilterData(Parcel in) {
        matchNames = in.createStringArrayList();
        name = in.readString();
        String name = in.readString();
        category = name.isEmpty() ? null : Category.valueOf(name);
        name = in.readString();
        ownerType = name.isEmpty() ? null : OwnerType.valueOf(name);
        radius = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(matchNames);
        dest.writeString(name);
        dest.writeString(category == null ? "" : category.name());
        dest.writeString(ownerType == null ? "" : ownerType.name());
        dest.writeInt(radius);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FilterData> CREATOR = new Creator<FilterData>() {
        @Override
        public FilterData createFromParcel(Parcel in) {
            return new FilterData(in);
        }

        @Override
        public FilterData[] newArray(int size) {
            return new FilterData[size];
        }
    };

    public List<String> getMatchNames() {
        return matchNames;
    }

    public void setMatchNames(List<String> matchNames) {
        this.matchNames = matchNames;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public OwnerType getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(OwnerType ownerType) {
        this.ownerType = ownerType;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
