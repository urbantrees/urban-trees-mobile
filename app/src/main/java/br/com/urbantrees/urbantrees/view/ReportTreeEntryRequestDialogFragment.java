package br.com.urbantrees.urbantrees.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.bean.Request;
import br.com.urbantrees.urbantrees.bean.Tree;
import br.com.urbantrees.urbantrees.bean.TreeEntry;
import br.com.urbantrees.urbantrees.bean.parcel.ReportEntryData;
import br.com.urbantrees.urbantrees.work.AccountWorker;
import br.com.urbantrees.urbantrees.work.RequestWorker;

public class ReportTreeEntryRequestDialogFragment extends DialogFragment {

    public static final String REQUEST_REPORT_ENTRY_DIALOG = "reportEntryRequestDialog";
    private static final String KEY_DATA = "data";

    private ReportEntryData data;
    private View dialogView;

    public static ReportTreeEntryRequestDialogFragment newInstance(ReportEntryData data) {
        ReportTreeEntryRequestDialogFragment fragment = new ReportTreeEntryRequestDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_DATA, this.data);
        super.onSaveInstanceState(outState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        loadSavedInstanceState(savedInstanceState);
        dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_report_tree_entry_request, null);
        setViews();
       return createDialog();
    }

    private void loadSavedInstanceState(Bundle savedInstanceState) {
        Bundle state = savedInstanceState == null ? getArguments() : savedInstanceState;
        this.data = state.getParcelable(KEY_DATA);
    }

    private void setViews() {
        ((TextView) dialogView.findViewById(R.id.textTreeName)).setText(this.data.getTreeName());
        ((TextView) dialogView.findViewById(R.id.textLatitude)).setText(this.data.getLat());
        ((TextView) dialogView.findViewById(R.id.textLongitude)).setText(this.data.getLng());
    }

    private AlertDialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.watta_happening_with_this_tree);
        builder.setView(dialogView);
        builder.setPositiveButton(R.string.send, new OkListener());
        builder.setNegativeButton(R.string.cancel, null);
        return builder.create();
    }

    private class OkListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            Request request = new Request();
            request.setSender(new AccountWorker(getActivity()).getGoogleAccount());

            Tree tree = new Tree();
            tree.setNicks(null);
            tree.setCategories(null);
            tree.setName(data.getTreeName());
            TreeEntry entry = new TreeEntry();
            entry.setTree(tree);
            entry.setLat(Double.valueOf(data.getLat()));
            entry.setLng(Double.valueOf(data.getLng()));
            request.setEntry(entry);

            EditText textMessage = (EditText) dialogView.findViewById(R.id.textMessage);
            request.setMessage(textMessage.getText().toString());

            new RequestWorker(getActivity()).sendRequest(request);
        }
    }

}
