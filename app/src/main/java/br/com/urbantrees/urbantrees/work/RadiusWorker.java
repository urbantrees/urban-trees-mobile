package br.com.urbantrees.urbantrees.work;

import android.app.Activity;
import android.location.Location;
import android.support.v4.content.ContextCompat;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.alfa.AlfaInterceptor;

public class RadiusWorker {

    private Activity activity;
    private GoogleMap mMap;
    private Location referenceLocation;
    private Circle radiusCircle;

    public RadiusWorker(Activity activity) {
        this.activity = activity;
        setSeekRadius();
        setTextKm();
    }

    public void setMMap(GoogleMap mMap) {
        this.mMap = mMap;
    }

    public void setReferenceLocation(Location referenceLocation) {
        this.referenceLocation = referenceLocation;
    }

    private Location getReferenceLocation() {
        return referenceLocation;
    }

    private void setSeekRadius() {
        SeekBar seekRadius = (SeekBar) activity.findViewById(R.id.seekRadius);
        seekRadius.setMax(activity.getResources().getInteger(R.integer.radius_max));
        seekRadius.setProgress(activity.getResources().getInteger(R.integer.radius_default));
        seekRadius.setOnSeekBarChangeListener((SeekBar.OnSeekBarChangeListener) activity);
    }

    private void setTextKm() {
        SeekBar seekRadius = (SeekBar) activity.findViewById(R.id.seekRadius);
        TextView textKm = (TextView) activity.findViewById(R.id.textKm);
        double aspect = seekRadius.getProgress() / 1000.;
        textKm.setText(activity.getString(R.string.text_km_radius, aspect));
    }

    public void refreshRadius() {
        setTextKm();

        if (getReferenceLocation() == null)
            return;

        if (radiusCircle != null)
            radiusCircle.remove();

        if (!AlfaInterceptor.isRadiusOn(activity))
            return;

        SeekBar seekRadius = (SeekBar) activity.findViewById(R.id.seekRadius);
        LatLng latLng = new LatLng(getReferenceLocation().getLatitude(), getReferenceLocation().getLongitude());

        radiusCircle = mMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(seekRadius.getProgress())
                .strokeColor(ContextCompat.getColor(activity, R.color.colorAccent)));

        radiusZoom(latLng);
    }

    private void radiusZoom(LatLng latLng) {
        // THAT'S A FUCKING EXPONENTIAL FUNCTION! BUT I DON'T GIVE A FUCK!

        SeekBar seekRadius = (SeekBar) activity.findViewById(R.id.seekRadius);
        int radius = seekRadius.getProgress();

        float fakeAspect = radius < 725 ? 4.5f : radius < 1250 ? 3.5f : radius < 2500 ? 3f : 2.5f;
        float zoom = 11 + (fakeAspect - ((radius / 10000.f) * fakeAspect));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

}
