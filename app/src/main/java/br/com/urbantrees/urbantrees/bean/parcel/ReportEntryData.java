package br.com.urbantrees.urbantrees.bean.parcel;

import android.os.Parcel;
import android.os.Parcelable;

public class ReportEntryData implements Parcelable {

    private String treeName;
    private String lat;
    private String lng;

    public ReportEntryData() {}

    protected ReportEntryData(Parcel in) {
        treeName = in.readString();
        lat = in.readString();
        lng = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(treeName);
        dest.writeString(lat);
        dest.writeString(lng);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ReportEntryData> CREATOR = new Creator<ReportEntryData>() {
        @Override
        public ReportEntryData createFromParcel(Parcel in) {
            return new ReportEntryData(in);
        }

        @Override
        public ReportEntryData[] newArray(int size) {
            return new ReportEntryData[size];
        }
    };

    public String getTreeName() {
        return treeName;
    }

    public void setTreeName(String treeName) {
        this.treeName = treeName;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
