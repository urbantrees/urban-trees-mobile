package br.com.urbantrees.urbantrees.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.alfa.AlfaInterceptor;
import br.com.urbantrees.urbantrees.work.AccountWorker;

public class SettingsActivity extends AppCompatActivity {

    private AccountWorker accountWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        this.accountWorker = new AccountWorker(this);

        setFields();
    }

    private void setFields() {
        refreshTextGoogleAccount();
        AlfaInterceptor.applyAlfaSettings(this);
    }

    public void refreshTextGoogleAccount() {
        TextView textGoogleAccount = (TextView) findViewById(R.id.textGoogleAccount);

        final String googleAccount = accountWorker.getGoogleAccount();
        textGoogleAccount.setText(googleAccount.isEmpty() ? getString(R.string.click_here_to_login) : googleAccount);

        textGoogleAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accountWorker.requestConfigureGoogleAccount(new AccountWorker.RemoveCallback() {
                    @Override
                    public void onDismiss() {
                        setFields();
                    }
                });
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        accountWorker.onActivityResult(requestCode, resultCode, data);
        setFields();
        super.onActivityResult(requestCode, resultCode, data);
    }
}
