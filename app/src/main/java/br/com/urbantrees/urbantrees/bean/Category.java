package br.com.urbantrees.urbantrees.bean;

import android.content.Context;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.urbantrees.urbantrees.R;

public enum Category {

    FRUIT("fruit", R.string.cagetory_fruit),
    HARDWOOD("hardwood", R.string.category_law),
    FLOWER("flower", R.string.category_flower),
    PALM("palm", R.string.category_palm);

    private String db;
    private final int id;

    Category(String db, int id) {
        this.db = db;
        this.id = id;
    }

    public String getDb() {
        return db;
    }

    public int getId() {
        return id;
    }

    @Nullable
    public static Category valueOfDb(String db) {
        for (Category category : values())
            if (category.getDb().equals(db))
                return category;
        return null;
    }

    @Nullable
    public static Category valueOf(Context context, String name) {
        for (Category category : values())
            if (context.getString(category.getId()).equals(name))
                return category;
        return null;
    }

    public static List<String> getNames(Context context) {
        return getNames(context, Arrays.asList(values()));
    }

    public static List<String> getNames(Context context, List<Category> categories) {
        List<String> names = new ArrayList<>();
        for (Category category : categories)
            names.add(context.getString(category.getId()));
        return names;
    }
}
