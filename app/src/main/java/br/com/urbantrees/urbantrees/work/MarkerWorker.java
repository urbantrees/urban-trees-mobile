package br.com.urbantrees.urbantrees.work;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.alfa.AlfaInterceptor;
import br.com.urbantrees.urbantrees.bean.Category;
import br.com.urbantrees.urbantrees.bean.TreeIcon;
import br.com.urbantrees.urbantrees.bean.parcel.FilterData;
import br.com.urbantrees.urbantrees.bean.Tree;
import br.com.urbantrees.urbantrees.bean.TreeEntry;
import br.com.urbantrees.urbantrees.json.JsonRequestParser;
import br.com.urbantrees.urbantrees.view.TreeEntryDetailsDialogFragment;

public class MarkerWorker implements GoogleMap.OnInfoWindowClickListener {

    private List<TreeEntry> entriesMarked = new ArrayList<>();
    private List<Marker> markers = new ArrayList<>();
    private Location referenceLocation;

    private AppCompatActivity activity;
    private GoogleMap mMap;

    public MarkerWorker(AppCompatActivity activity) {
        this.activity = activity;
    }

    public void setMMap(GoogleMap mMap) {
        this.mMap = mMap;
        this.mMap.setOnInfoWindowClickListener(this);
    }

    public void setReferenceLocation(Location referenceLocation) {
        this.referenceLocation = referenceLocation;
    }

    public Location getReferenceLocation() {
        return referenceLocation;
    }

    public void refreshMarkers(FilterData filterData) {
        if (getReferenceLocation() == null)
            return;

        Toast.makeText(activity, R.string.refreshing, Toast.LENGTH_SHORT).show();

        requestTreeEntries(referenceLocation, filterData);
    }

    private void requestTreeEntries(Location location, FilterData filterData) {
        new TreeEntryWorker(activity).requestTreeEntries(location, filterData, new TreeEntryWorker.RequestTreeEntryCallback() {
            @Override
            public void onCallback(List<TreeEntry> entries) {
                if (entries.isEmpty())
                    Toast.makeText(activity, "Sem árvores no local!", Toast.LENGTH_SHORT).show();

                updateMarkers(entries);
            }
        });
    }

    private void updateMarkers(List<TreeEntry> entries) {
        this.entriesMarked = entries;

        addMarkersOnTheLine();
        removeOldMarkers();
    }

    private void addMarkersOnTheLine() {
        for (TreeEntry entry : entriesMarked)
            if (!hasMarked(entry))
                addMarker(entry);
    }

    private void addMarker(TreeEntry entry) {
        MarkerOptions markerOptions = new MarkerOptions()
                .position(new LatLng(entry.getLat(), entry.getLng()))
                .title(entry.getTree().getName())
                .snippet(entry.getTree().getScientificName())
                .icon(getIcon(entry).getBitmapDescriptor());

        Marker marker = mMap.addMarker(markerOptions);
        markers.add(marker);
    }

    @NonNull
    private TreeIcon getIcon(TreeEntry entry) {
        TreeIcon icon = entry.getTree().getIcon();
        if (icon != null)
            return icon;

        if (entry.getTree().getCategories().contains(Category.PALM))
            return TreeIcon.PALM_GREEN;
        if (entry.getTree().getCategories().contains(Category.FRUIT))
            return TreeIcon.GREEN_FRUIT_RED;
        if (entry.getTree().getCategories().contains(Category.HARDWOOD))
            return TreeIcon.DARK_GREEN;

        return TreeIcon.GREEN;
    }

    private boolean hasMarked(TreeEntry entry) {
        for(Marker marker : markers) {
            LatLng p = marker.getPosition();
            if (p.latitude == entry.getLat() && p.longitude == entry.getLng())
                return true;
        }

        return false;
    }

    private void removeOldMarkers() {
        List<Marker> ok = new ArrayList<>();

        for (Marker marker : markers) {
            LatLng p = marker.getPosition();
            for(TreeEntry entry : entriesMarked)
                if (p.latitude == entry.getLat() && p.longitude == entry.getLng())
                    ok.add(marker);
        }

        ArrayList<Marker> toRemove = new ArrayList<>(this.markers);
        toRemove.removeAll(ok);

        for (Marker marker : toRemove)
            marker.remove();

        this.markers.removeAll(toRemove);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        LatLng p = marker.getPosition();
        for(final TreeEntry entry : entriesMarked)
            if (p.latitude == entry.getLat() && p.longitude == entry.getLng()) {
                TreeEntryDetailsDialogFragment dialog = TreeEntryDetailsDialogFragment.newInstance(activity, entry);
                dialog.show(activity.getSupportFragmentManager(), TreeEntryDetailsDialogFragment.TREE_ENTRY_DIALOG);
                break;
            }
    }
}
