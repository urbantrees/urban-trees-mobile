package br.com.urbantrees.urbantrees.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.bean.Request;
import br.com.urbantrees.urbantrees.work.AccountWorker;
import br.com.urbantrees.urbantrees.work.RequestWorker;

public class NewTreeRequestDialogFragment extends DialogFragment {

    public static final String REQUEST_TREE_DIALOG = "newTreeRequestDialog";

    private View dialogView;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_new_tree_request, null);
       return createDialog();
    }

    private AlertDialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.where_is_my_tree);
        builder.setView(dialogView);
        builder.setPositiveButton(R.string.send, new OkListener());
        builder.setNegativeButton(R.string.cancel, null);
        return builder.create();
    }

    private class OkListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            Request request = new Request();
            request.setSender(new AccountWorker(getActivity()).getGoogleAccount());

            EditText textMessage = (EditText) dialogView.findViewById(R.id.textMessage);
            request.setMessage(textMessage.getText().toString());

            new RequestWorker(getActivity()).sendRequest(request);
        }
    }

}
