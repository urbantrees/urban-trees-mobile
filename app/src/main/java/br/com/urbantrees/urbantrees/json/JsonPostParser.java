package br.com.urbantrees.urbantrees.json;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.bean.Ack;

public class JsonPostParser extends JsonRequestParser<Ack> {

    public JsonPostParser(final Context context, String url, Object request, final int successMessage) {
        super(Method.POST, url, request, new Response.Listener<Ack>() {
            @Override
            public void onResponse(Ack response) {
                if (response.isOk()) {
                    Toast.makeText(context, successMessage, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, response.getStatus(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, R.string.try_again, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected Class<Ack> getParseClass() {
        return Ack.class;
    }
}
