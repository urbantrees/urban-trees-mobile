package br.com.urbantrees.urbantrees.bean;

import android.content.Context;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import br.com.urbantrees.urbantrees.R;

public enum OwnerType {
    PUBLIC("public", R.string.owner_type_public),
    PRIVATE("private", R.string.owner_type_private);

    private String db;
    private int id;

    OwnerType(String db, int id) {
        this.db = db;
        this.id = id;
    }

    public String getDb() {
        return db;
    }

    public int getId() {
        return id;
    }

    public String stringResource(Context context) {
        return context.getString(getId());
    }

    @Nullable
    public static OwnerType valueOfDb(String db) {
        for (OwnerType type : values())
            if (type.getDb().equals(db))
                return type;
        return null;
    }

    @Nullable
    public static OwnerType valueOf(Context context, String name) {
        for (OwnerType type : values())
            if (context.getString(type.getId()).equals(name))
                return type;
        return null;
    }

    public static List<String> getNames(Context context) {
        List<String> names = new ArrayList<>();
        for (OwnerType type : values())
            names.add(context.getString(type.getId()));
        return names;
    }
}
