package br.com.urbantrees.urbantrees.work;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.google.android.gms.common.AccountPicker;

import br.com.urbantrees.urbantrees.R;

public class AccountWorker {

    private static final String KEY_GOOGLE_ACCOUNT = "GOOGLE_ACCOUNT";
    private static final int REQUEST_CODE_PICK_ACCOUNT = 1000;

    private Activity activity;

    public AccountWorker(Activity activity) {
        this.activity = activity;
    }

    public void requestConfigureGoogleAccount() {
        requestConfigureGoogleAccount(null);
    }

    public void requestConfigureGoogleAccount(final RemoveCallback callback) {
        if (!getGoogleAccount().isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(getGoogleAccount());
            builder.setPositiveButton(R.string.change, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    selectAccount();
                }
            });
            builder.setNegativeButton(R.string.remove, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    setGoogleAccount("");
                    if (callback != null)
                        callback.onDismiss();
                }
            });

            builder.show();
        } else {
            selectAccount();
        }
    }

    private void selectAccount() {
        Intent intent = AccountPicker.newChooseAccountIntent(null, null, new String[]{"com.google"}, false, null, null, null, null);
        activity.startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
    }

    @NonNull
    public String getGoogleAccount() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        return sharedPreferences.getString(KEY_GOOGLE_ACCOUNT, "");
    }

    private void setGoogleAccount(String googleAccount) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(KEY_GOOGLE_ACCOUNT, googleAccount);
        editor.apply();
    }

    private SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(activity);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != REQUEST_CODE_PICK_ACCOUNT)
            return;

        if (resultCode == activity.RESULT_OK)
            setGoogleAccount(data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME));
    }

    public boolean validateGoogleAccount() {
        return validateGoogleAccount(activity.findViewById(R.id.fabFilter));
    }

    public boolean validateGoogleAccount(View view) {
        if (getGoogleAccount().isEmpty()) {
            Snackbar.make(view, R.string.you_dont_have_google_account,
                    Snackbar.LENGTH_LONG).setAction(R.string.please_configure, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    requestConfigureGoogleAccount();
                }
            }).show();

            return false;
        } else
            return true;
    }

    public interface RemoveCallback {
        void onDismiss();
    }
}
