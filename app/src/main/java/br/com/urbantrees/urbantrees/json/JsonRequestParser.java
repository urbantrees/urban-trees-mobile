package br.com.urbantrees.urbantrees.json;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

public abstract class JsonRequestParser<T> extends JsonRequest<T> {

    private static final Gson gson = GsonComposer.compose();

    public JsonRequestParser(int method, String url, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, "", listener, errorListener);
    }

    public JsonRequestParser(int method, String url, Object request, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, gson.toJson(request), listener, errorListener);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, PROTOCOL_CHARSET);
            return Response.success(gson.fromJson(json, getParseClass()), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new VolleyError(response));
        }
    }

    protected abstract Class<T> getParseClass();
}
