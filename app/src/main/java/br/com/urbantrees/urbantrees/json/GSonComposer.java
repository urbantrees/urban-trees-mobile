package br.com.urbantrees.urbantrees.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Date;

import br.com.urbantrees.urbantrees.bean.Category;
import br.com.urbantrees.urbantrees.bean.OwnerType;
import br.com.urbantrees.urbantrees.bean.TreeIcon;

public final class GsonComposer {

    public static Gson compose() {
        return new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateSerializer())
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .registerTypeAdapter(Category.class, new CategorySerializer())
                .registerTypeAdapter(Category.class, new CategoryDeserializer())
                .registerTypeAdapter(OwnerType.class, new OwnerTypeSerializer())
                .registerTypeAdapter(OwnerType.class, new OwnerTypeDeserializer())
                .registerTypeAdapter(TreeIcon.class, new TreeIconSerializer())
                .registerTypeAdapter(TreeIcon.class, new TreeIconDeserializer())
                .setPrettyPrinting().create();
    }

    private static class DateSerializer implements JsonSerializer<Date> {
        @Override
        public JsonElement serialize(Date date, Type typeOfSrc, JsonSerializationContext context) {
            long time = date.getTime();
            return new JsonPrimitive(time);
        }
    }

    private static class DateDeserializer implements JsonDeserializer<Date> {
        @Override
        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return new Date(json.getAsLong());
        }
    }

    private static class CategorySerializer implements JsonSerializer<Category> {
        @Override
        public JsonElement serialize(Category category, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(category.getDb());
        }
    }

    private static class CategoryDeserializer implements JsonDeserializer<Category> {
        @Override
        public Category deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return Category.valueOfDb(json.getAsString());
        }
    }

    private static class OwnerTypeSerializer implements JsonSerializer<OwnerType> {
        @Override
        public JsonElement serialize(OwnerType ownerType, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(ownerType.getDb());
        }
    }

    private static class OwnerTypeDeserializer implements JsonDeserializer<OwnerType> {
        @Override
        public OwnerType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return OwnerType.valueOfDb(json.getAsString());
        }
    }

    private static class TreeIconSerializer implements JsonSerializer<TreeIcon> {
        @Override
        public JsonElement serialize(TreeIcon icon, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(icon.getDb());
        }
    }

    private static class TreeIconDeserializer implements JsonDeserializer<TreeIcon> {
        @Override
        public TreeIcon deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return TreeIcon.valueOfDb(json.getAsString());
        }
    }
}
