package br.com.urbantrees.urbantrees.bean;

public class Ack {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isOk() {
        return "OK".equals(this.status);
    }
}
