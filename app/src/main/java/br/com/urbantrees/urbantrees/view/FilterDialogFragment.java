package br.com.urbantrees.urbantrees.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.bean.Category;
import br.com.urbantrees.urbantrees.bean.parcel.FilterData;
import br.com.urbantrees.urbantrees.bean.OwnerType;

public class FilterDialogFragment extends DialogFragment {

    public static final String FILTER_DIALOG_TAG = "filterDialog";
    private static final String KEY_SAVED_DATA = "matchNames";

    private FilterData savedData;
    private View dialogView;

    public static FilterDialogFragment newInstance(FilterData data) {
        FilterDialogFragment fragment = new FilterDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_SAVED_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_SAVED_DATA, this.savedData);
        super.onSaveInstanceState(outState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        loadSavedInstanceState(savedInstanceState);
        dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_tree_filter, null);
        setViews();
       return createDialog();
    }

    private void loadSavedInstanceState(Bundle savedInstanceState) {
        Bundle state = savedInstanceState == null ? getArguments() : savedInstanceState;
        this.savedData = state.getParcelable(KEY_SAVED_DATA);
    }

    private void setViews() {
        setSpinnerCategories();
        setTextFilter();
        setSpinnerOwnerType();
    }

    private void setSpinnerCategories() {
        Category category = savedData.getCategory();
        setSpinnerValues(R.id.spinnerCategory, Category.getNames(getContext()), category == null ? null : category.getId());
    }

    private void setTextFilter() {
        AutoCompleteTextView textFilter = (AutoCompleteTextView) dialogView.findViewById(R.id.textFilter);
        ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, savedData.getMatchNames());
        textFilter.setAdapter(adapter);

        if (savedData.getName() != null)
            textFilter.setText(savedData.getName());
    }

    private void setSpinnerOwnerType() {
        OwnerType ownerType = savedData.getOwnerType();
        setSpinnerValues(R.id.spinnerOwnerType, OwnerType.getNames(getContext()), ownerType == null ? null : ownerType.getId());
    }

    private void setSpinnerValues(int spinnerId, List<String> typeValues, Integer selection) {
        Spinner spinner = (Spinner) dialogView.findViewById(spinnerId);
        List<String> values = new ArrayList<>();
        values.add(getString(R.string.all));
        values.addAll(typeValues);
        ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, values);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        if (selection != null)
            spinner.setSelection(adapter.getPosition(getString(selection)));
    }

    private AlertDialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialogView);
        builder.setPositiveButton(R.string.filter, new FilterListener());
        builder.setNeutralButton(R.string.clear, new ClearListener());
        builder.setNegativeButton(R.string.cancel, null);

        return builder.create();
    }

    private class FilterListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            AutoCompleteTextView textFilter = (AutoCompleteTextView) dialogView.findViewById(R.id.textFilter);
            Spinner spinnerCategory = (Spinner) dialogView.findViewById(R.id.spinnerCategory);
            Spinner spinnerOwnerType = (Spinner) dialogView.findViewById(R.id.spinnerOwnerType);

            savedData.setName(textFilter.getText().toString());
            savedData.setCategory(Category.valueOf(getContext(), spinnerCategory.getSelectedItem().toString()));
            savedData.setOwnerType(OwnerType.valueOf(getContext(), spinnerOwnerType.getSelectedItem().toString()));

            ((DismissCallback)getActivity()).onDismiss(savedData);
        }
    }

    private class ClearListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            savedData.setName(null);
            savedData.setCategory(null);
            savedData.setOwnerType(null);

            ((DismissCallback)getActivity()).onDismiss(savedData);
        }
    }

    public interface DismissCallback {
        void onDismiss (FilterData data);
    }

}
