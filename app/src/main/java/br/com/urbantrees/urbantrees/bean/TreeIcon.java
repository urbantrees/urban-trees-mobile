package br.com.urbantrees.urbantrees.bean;

import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import br.com.urbantrees.urbantrees.R;

public enum TreeIcon {

    //NORMAL TREES
    GREEN("Tgreen", R.mipmap.tree_green),
    DARK_GREEN("Tdkgreen", R.mipmap.tree_dark_green),
    LIGHT_GREEN("Tltgreen", R.mipmap.tree_light_green),
    LIGHT_PINK("Tltpink", R.mipmap.tree_light_pink),
    LIGHT_YELLOW("Tltyellow", R.mipmap.tree_light_yellow),
    PINK("Tpink", R.mipmap.tree_pink),
    YELLOW("Tyellow", R.mipmap.tree_yellow),
    PURPLE("Tpurple", R.mipmap.tree_purple),
    WHITE("Twhite", R.mipmap.tree_white),

    //FRUIT TREES
    GREEN_FRUIT_DARK_GREEN("TgreenFRdkgreen", R.mipmap.tree_green_fruit_dark_green),
    GREEN_FRUIT_LIGHT_GREEN("TgreenFRltgreen", R.mipmap.tree_green_fruit_light_green),
    GREEN_FRUIT_LIGHT_YELLOW("TgreenFRltyellow", R.mipmap.tree_green_fruit_light_yellow),
    GREEN_FRUIT_ORANGE("TgreenFRorange", R.mipmap.tree_green_fruit_orange),
    GREEN_FRUIT_PINK("TgreenFRpink", R.mipmap.tree_green_fruit_pink),
    GREEN_FRUIT_RED("TgreenFRred", R.mipmap.tree_green_fruit_red),
    GREEN_FRUIT_WINE("TgreenFRwine", R.mipmap.tree_green_fruit_wine),
    GREEN_FRUIT_YELLOW("TgreenFRyellow", R.mipmap.tree_green_fruit_yellow),

    //FLOWER TREES
    GREEN_FLOWER_PINK("TgreenFLpink", R.mipmap.tree_green_flower_pink),
    GREEN_FLOWER_PURPLE("TgreenFLpurple", R.mipmap.tree_green_flower_purple),
    GREEN_FLOWER_RED("TgreenFLred", R.mipmap.tree_green_flower_red),
    GREEN_FLOWER_WHITE("TgreenLFwhite", R.mipmap.tree_green_flower_white),
    GREEN_FLOWER_YELLOW("TgreenFLyellow", R.mipmap.tree_green_flower_yellow),

    //PALM TREES

    PALM_GREEN("Pgreen", R.mipmap.palm_green),
    PALM_GREEN_FRUIT_BROWN("PgreenFRbrown", R.mipmap.palm_green_fruit_brown),
    PALM_GREEN_FRUIT_GREEN("PgreenFRgreen", R.mipmap.palm_green_fruit_green),
    PALM_GREEN_FRUIT_ORANGE("PgreenFRorange", R.mipmap.palm_green_fruit_orange);

    private String db;
    private final int id;

    TreeIcon(String db, int id) {
        this.db = db;
        this.id = id;
    }

    public String getDb() {
        return db;
    }

    private int getId() {
        return id;
    }

    public BitmapDescriptor getBitmapDescriptor() {
        return BitmapDescriptorFactory.fromResource(getId());
    }

    @Nullable
    public static TreeIcon valueOfDb(String db) {
        for (TreeIcon icon : values())
            if (icon.getDb().equals(db))
                return icon;

        return GREEN;
    }

}
