package br.com.urbantrees.urbantrees.bean.parcel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class NewTreeEntryData implements Parcelable {

    private List<String> matchNames;
    private String name;
    private boolean iDunnoTheTree;
    private String ownerType;
    private LatLng latLng;

    public NewTreeEntryData(){}

    protected NewTreeEntryData(Parcel in) {
        matchNames = in.createStringArrayList();
        name = in.readString();
        iDunnoTheTree = in.readByte() != 0;
        ownerType = in.readString();
        latLng = in.readParcelable(LatLng.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(matchNames);
        dest.writeString(name);
        dest.writeByte((byte) (iDunnoTheTree ? 1 : 0));
        dest.writeString(ownerType);
        dest.writeParcelable(latLng, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NewTreeEntryData> CREATOR = new Creator<NewTreeEntryData>() {
        @Override
        public NewTreeEntryData createFromParcel(Parcel in) {
            return new NewTreeEntryData(in);
        }

        @Override
        public NewTreeEntryData[] newArray(int size) {
            return new NewTreeEntryData[size];
        }
    };

    public List<String> getMatchNames() {
        return matchNames;
    }

    public void setMatchNames(List<String> matchNames) {
        this.matchNames = matchNames;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public boolean isiDunnoTheTree() {
        return iDunnoTheTree;
    }

    public void setiDunnoTheTree(boolean iDunnoTheTree) {
        this.iDunnoTheTree = iDunnoTheTree;
    }
}
