package br.com.urbantrees.urbantrees.work;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.alfa.AlfaInterceptor;
import br.com.urbantrees.urbantrees.bean.Tree;
import br.com.urbantrees.urbantrees.json.JsonRequestParser;

public class TreeWorker {

    private List<Tree> trees = Collections.EMPTY_LIST;
    private Context context;

    public TreeWorker(Context context) {
        this.context = context;
    }

    public List<Tree> getTrees() {
        return trees;
    }

    public List<String> getTreeNamesAndNicks() {
        List<String> values = new ArrayList<>();
        for (Tree tree : getTrees()) {
            values.add(tree.getName());
            values.addAll(tree.getNicks());
        }
        return values;
    }

    public Tree getNormalTree() {
        Tree tree = new Tree();
        tree.setName("Arvore Comum");
        tree.setScientificName("");
        return tree;
    }

    public Tree getTree(String match) {
        for (Tree tree : getTrees())
            if (treeMatches(tree, match))
                return tree;
        return null;
    }

    private boolean treeMatches(Tree tree, String match) {
        return tree.getName().equals(match) || tree.getNicks().contains(match);
    }

    public void requestTrees(final RequestTreesCallback callback) {
        if (!getTrees().isEmpty()) {
            if (callback != null)
                callback.go();
            return;
        }

        Toast.makeText(context, R.string.loading_trees, Toast.LENGTH_SHORT).show();

        //context.getResources().getString(R.string.get_trees_url);
        String url = AlfaInterceptor.getTreesUrl(context);
        final RequestQueue queue = Volley.newRequestQueue(context);

        queue.add(
                new JsonRequestParser<Tree[]>(Request.Method.GET, url, new Response.Listener<Tree[]>() {
                    @Override
                    public void onResponse(Tree[] response) {
                        TreeWorker.this.trees = Arrays.asList(response);
                        if (callback != null)
                            callback.go();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, R.string.try_again, Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    protected Class<Tree[]> getParseClass() {
                        return Tree[].class;
                    }
                });

        queue.start();
    }

    public interface RequestTreesCallback {
        void go();
    }
}
