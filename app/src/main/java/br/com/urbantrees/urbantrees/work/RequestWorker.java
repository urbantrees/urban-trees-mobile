package br.com.urbantrees.urbantrees.work;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.json.JsonPostParser;

public class RequestWorker {

    private Context context;

    public RequestWorker(Context context) {
        this.context = context;
    }

    public void sendRequest(br.com.urbantrees.urbantrees.bean.Request request) {
        Toast.makeText(context, "Sending Request...", Toast.LENGTH_SHORT).show();

        String url = context.getResources().getString(R.string.put_request_url);
        final RequestQueue queue = Volley.newRequestQueue(context);

        queue.add(new JsonPostParser(context, url, request, R.string.request_sent));
        queue.start();
    }
}
