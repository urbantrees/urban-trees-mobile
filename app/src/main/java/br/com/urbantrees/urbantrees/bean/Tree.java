package br.com.urbantrees.urbantrees.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Tree {

    private String name;
    private String scientificName;
    private List<Category> categories = new ArrayList<>();
    private List<String> nicks = new ArrayList<>();
    private TreeIcon icon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScientificName() {
        return scientificName;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setNicks(List<String> nicks) {
        this.nicks = nicks;
    }

    public List<String> getNicks() {
        return nicks;
    }

    public TreeIcon getIcon() {
        return icon;
    }

    public void setIcon(TreeIcon icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return name;
    }
}
