package br.com.urbantrees.urbantrees.alfa;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.Arrays;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.work.AccountWorker;

public class AlfaInterceptor {

    private static final String KEY_ALFA_ACCOUNT = "ALFA_ACCOUNT";
    private static final String KEY_RADIUS_ON = "RADIUS_ON";

    public static void applyAlfaSettings(final Activity activity) {
        if (!isAlfaAccount(new AccountWorker(activity).getGoogleAccount())) {
            if (isAlfaOn(activity))
                setAlfa(activity, false);
            return;
        }

        LinearLayout contentView = (LinearLayout) activity.findViewById(R.id.activity_settings);
        final Button button = createButtonBaseAlfa(activity);
        contentView.addView(button);
        Button buttonNoRadius = createButtonNoRadius(activity);
        contentView.addView(buttonNoRadius);
    }

    @NonNull
    private static Button createButtonBaseAlfa(final Activity activity) {
        final Button button = new Button(activity);
        button.setText(isAlfaOn(activity) ? "BASE ALFA ON" : "BASE ALFA OFF");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final boolean alfaOn = isAlfaOn(activity);
                setAlfa(activity, !alfaOn);
                button.setText(!alfaOn ? "BASE ALFA ON" : "BASE ALFA OFF");
                Toast.makeText(activity, "REFRESH TO APPLY!", Toast.LENGTH_SHORT).show();
            }
        });
        return button;
    }

    @NonNull
    private static Button createButtonNoRadius(final Activity activity) {
        final Button button = new Button(activity);
        button.setText(isRadiusOn(activity) ? "RADIUS ON" : "RADIUS OFF");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final boolean radiusOn = isRadiusOn(activity);
                setRadiusOn(activity, !radiusOn);
                button.setText(!radiusOn ? "RADIUS ON" : "RADIUS OFF");
                Toast.makeText(activity, "REFRESH TO APPLY!", Toast.LENGTH_SHORT).show();
            }
        });
        return button;
    }

    private static boolean isAlfaAccount(String googleAccount) {
        return Arrays.asList(
                "pilovieira@gmail.com",
                "anderzanic@gmail.com",
                "gabriel.hpsilva@gmail.com",
                "marcelo@marcelotomazini.com").contains(googleAccount);
    }

    private static boolean isAlfaOn(Context c) {
        SharedPreferences sharedPreferences = getSharedPreferences(c);
        return sharedPreferences.getBoolean(KEY_ALFA_ACCOUNT, false);
    }

    private static void setAlfa(Context c, boolean alfaDb) {
        SharedPreferences.Editor editor = getSharedPreferences(c).edit();
        editor.putBoolean(KEY_ALFA_ACCOUNT, alfaDb);
        editor.apply();
    }

    public static boolean isRadiusOn(Context c) {
        SharedPreferences sharedPreferences = getSharedPreferences(c);
        return sharedPreferences.getBoolean(KEY_RADIUS_ON, true);
    }

    private static void setRadiusOn(Context c, boolean radiusOn) {
        SharedPreferences.Editor editor = getSharedPreferences(c).edit();
        editor.putBoolean(KEY_RADIUS_ON, radiusOn);
        editor.apply();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static int getRadius(Context c, int radius) {
        return isRadiusOn(c) ? radius : 999999999;
    }

    public static String getTreesUrl(Context context) {
        /*if (isAlfaOn(context))
            return "http://urbantreestests.herokuapp.com/tree";*/

        return context.getResources().getString(R.string.get_trees_url);
    }

    public static String postTreeEntryUrl(Context context) {
        if (isAlfaOn(context))
            return "http://urbantreestests.herokuapp.com/treeEntry";

        return context.getResources().getString(R.string.put_tree_entry_url);
    }

    public static String getTreeEntriesUrl(Context context, String lat, String lng, int radius) {
        if (isAlfaOn(context))
            return String.format("http://urbantreestests.herokuapp.com/treeEntry?lat=%s&lng=%s&radius=%s", lat, lng, radius);

        return context.getResources().getString(R.string.get_tree_entries_url, lat, lng, radius);
    }

}
