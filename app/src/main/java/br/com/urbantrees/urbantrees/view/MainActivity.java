package br.com.urbantrees.urbantrees.view;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import br.com.urbantrees.urbantrees.R;
import br.com.urbantrees.urbantrees.bean.parcel.FilterData;
import br.com.urbantrees.urbantrees.bean.parcel.NewTreeEntryData;
import br.com.urbantrees.urbantrees.work.AccountWorker;
import br.com.urbantrees.urbantrees.work.FabAnimateWorker;
import br.com.urbantrees.urbantrees.work.MarkerWorker;
import br.com.urbantrees.urbantrees.work.RadiusWorker;
import br.com.urbantrees.urbantrees.work.TreeEntryWorker;
import br.com.urbantrees.urbantrees.work.TreeWorker;

public class MainActivity extends AppCompatActivity implements
        OnMapReadyCallback, FilterDialogFragment.DismissCallback,
        NewTreeEntryDialogFragment.DismissCallback, View.OnClickListener,
        SeekBar.OnSeekBarChangeListener {

    private static final String KEY_FILTER_DATA = "FILTER_DATA";
    private static final int LOCATION_PERMISSION_TAG = 2016;

    private FilterData filterData;
    private AccountWorker accountWorker;
    private TreeWorker treeWorker;
    private MarkerWorker markerWorker;
    private RadiusWorker radiusWorker;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeWorkers();
        setFabs();

        configureMap();
        gimmeLocationPermission();
    }

    private void initializeWorkers() {
        accountWorker = new AccountWorker(this);
        treeWorker = new TreeWorker(this);
        markerWorker = new MarkerWorker(this);
        radiusWorker = new RadiusWorker(this);
    }

    private void setFabs() {
        findViewById(R.id.fabFilter).setOnClickListener(this);
        findViewById(R.id.fabRefresh).setOnClickListener(this);
        findViewById(R.id.fabNewTreeEntry).setOnClickListener(this);

        new FabAnimateWorker(this).prepare();
    }

    private void configureMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mainMap);
        mapFragment.getMapAsync(this);
    }

    private void gimmeLocationPermission() {
        if (!hasLocationPermission())
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_TAG);
    }

    private boolean hasLocationPermission() {
        return ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_TAG &&
                permissions.length == 1 &&
                permissions[0].equals(android.Manifest.permission.ACCESS_FINE_LOCATION) &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED)
            setMyLocationEnabled();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_settings)
            startActivity(new Intent(this, SettingsActivity.class));

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        setMMap(googleMap);
        markerWorker.setMMap(googleMap);
        radiusWorker.setMMap(googleMap);

        setMyLocationEnabled();
        setOnMyLocationListener();
    }

    private void setMMap(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));
        mMap.setBuildingsEnabled(false);
        mMap.setTrafficEnabled(false);
        mMap.getUiSettings().setCompassEnabled(true);
    }

    private void setMyLocationEnabled() {
        if (mMap == null)
            return;

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            mMap.setMyLocationEnabled(true);
    }

    private void setOnMyLocationListener() {
        //o correto seria implementar outra parada, mas não to com tempo
        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                if (markerWorker.getReferenceLocation() != null && markerWorker.getReferenceLocation().distanceTo(location) < 20)
                    return;

                markerWorker.setReferenceLocation(location);
                radiusWorker.setReferenceLocation(location);
                refresh();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_FILTER_DATA, getFilterData());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        setFilterData((FilterData) savedInstanceState.getParcelable(KEY_FILTER_DATA));
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.fabFilter)
            openFilterDialog();
        if (id == R.id.fabRefresh)
            refresh();
        if (id == R.id.fabNewTreeEntry)
            openNewTreeEntryDialog();
    }

    private void refresh() {
        markerWorker.refreshMarkers(getFilterData());
        radiusWorker.refreshRadius();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        accountWorker.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void openFilterDialog() {
        treeWorker.requestTrees(new TreeWorker.RequestTreesCallback() {
            @Override
            public void go() {
                getFilterData().setMatchNames(treeWorker.getTreeNamesAndNicks());
                FilterDialogFragment dialog = FilterDialogFragment.newInstance(getFilterData());
                dialog.show(getSupportFragmentManager(), FilterDialogFragment.FILTER_DIALOG_TAG);
            }
        });
    }

    @Override
    public void onDismiss(FilterData data) {
        setFilterData(data);
        refresh();
    }

    private void openNewTreeEntryDialog() {
        NewTreeEntryData data = new NewTreeEntryData();
        data.setMatchNames(treeWorker.getTreeNamesAndNicks());
        data.setLatLng(new LatLng(markerWorker.getReferenceLocation().getLatitude(), markerWorker.getReferenceLocation().getLongitude()));
        openNewTreeEntryDialog(data);
    }

    private void openNewTreeEntryDialog(final NewTreeEntryData data) {
        if (!hasLocationPermission()) {
            gimmeLocationPermission();
            return;
        }

        treeWorker.requestTrees(new TreeWorker.RequestTreesCallback() {
            @Override
            public void go() {
                NewTreeEntryDialogFragment dialog = NewTreeEntryDialogFragment.newInstance(data);
                dialog.show(getSupportFragmentManager(), NewTreeEntryDialogFragment.NEW_TREE_ENTRY_DIALOG);
            }
        });
    }

    @Override
    public void onDismiss(NewTreeEntryData data) {
        try {
            new TreeEntryWorker(this).createNewTreeEntry(data, treeWorker);
        } catch (RuntimeException ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {}

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        getFilterData().setRadius(seekBar.getProgress());
        refresh();
    }

    private FilterData getFilterData() {
        if (filterData == null) {
            filterData = new FilterData();
            filterData.setRadius(getResources().getInteger(R.integer.radius_default));
        }
        return filterData;
    }

    private void setFilterData(FilterData filterData) {
        this.filterData = filterData;
    }

}
